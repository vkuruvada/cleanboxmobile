'use strict';

/**
 * Created by venkatakuruvada on 10/8/14.
 */

angular.module('CleanBoxMobile.all', ['CleanBoxMobile.menu',
                                      'CleanBoxMobile.auth',
                                      'CleanBoxMobile.home',
                                      'CleanBoxMobile.settings',
                                      'CleanBoxMobile.move',
                                      'CleanBoxMobile.stats',
                                      'CleanBoxMobile.common',
                                      'CleanBoxMobile.help']);
