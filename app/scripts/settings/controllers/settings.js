'use strict';

angular.module('CleanBoxMobile.settings',[])
    .controller('SettingsCtrl', function($scope, User, Auth, $location, Accounts , Socket, filter){


        var setting = this;
        this.errors = {};
        $scope.user= {};
        this.deleteInProgress = null;
        this.settingFilter=false;
        this.filter= {
          value: (filter.data && filter.data.filter) ||"ALL"
        };

        this.changePassword = function(form) {
            this.submitted = true;

            if(form.$valid) {
                Auth.changePassword( $scope.user.oldPassword, $scope.user.newPassword )
                    .then( function() {
                        setting.message = 'Password successfully changed.';
                    })
                    .catch( function() {
                        form.oldPassword.$setValidity('mongoose', false);
                        setting.errors.other = 'Incorrect password';
                    });
            }
        };


      this.setFilter = function(filterform) {


        setting.settingFilter=true;
        if(filterform.$valid) {
          Auth.setFilter( setting.filter.value )
            .then( function() {
              setting.settingFilter=false;
            })
            .catch( function() {
              setting.settingFilter=false;
            });
        }

      };

        this.accounts = Accounts.getUserAccounts();

        this.onAddAccount = function() {
            $location.path('/auth/google');
        };

        this.onDeleteAccount = function(account) {

            Socket.emit('delete:account',{
                user :$scope.currentUser,
                account : account
            });

            this.deleteInProgress = account._id;
        };


        this.onDeleteSuccess  = function(account) {

            $scope.$apply(function(){

                setting.accounts = setting.accounts.filter(function(acc){

                    if(acc._id != account._id) {
                        return true;
                    }
                    return false;
                });

                Accounts.updateAccounts(setting.accounts);
                setting.deleteInProgress = null;
            })
        };

        Socket.on('delete:account:success', this.onDeleteSuccess, this);
    });
