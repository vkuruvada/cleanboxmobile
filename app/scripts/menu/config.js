'use strict';

/**
 * Created by venkatakuruvada on 10/19/14.
 */

angular.module('CleanBoxMobile.menu',[])
    .config(function($stateProvider, $urlRouterProvider){


        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu/menu.html',
                cache:false,
                controller: 'MenuCtrl as menu'
            })
            .state('app.settings', {
                url: '/settings',
                cache:false,
                views: {
                    'menuContent' :{
                        templateUrl: 'templates/settings/settings.html',
                        controller:'SettingsCtrl as setting',
                        cache:false,
                        resolve : {
                            accounts : ['Accounts',function(Accounts) {
                                return Accounts.getUserAccounts();
                            }],
                            connection: ['Socket', function(Socket) {
                                return Socket.connect();
                            }],
                            filter: ['Auth', function(Auth) {
                              return Auth.getFilter();
                            }]

                        }
                    }
                }
            })

            .state('app.stats', {
                url: '/stats',
                views: {
                    'menuContent' :{
                        templateUrl: 'templates/stats/accounts.html',
                        controller:'AccountsStatCtrl as stats',
                        resolve : {
                            accounts : ['Accounts',function(Accounts) {
                                return Accounts.getUserAccounts();
                            }]
                        }
                    }
                }
            })

            .state('app.history', {
                url: '/stats/:id',
                views: {
                    'menuContent' :{
                        templateUrl: 'templates/stats/details.html',
                        controller:'DetailsCtrl as details',
                        resolve : {
                            accounts : ['Accounts',function(Accounts) {
                                return Accounts.getUserAccounts();
                            }]
                        }
                    }
                }
            })

            .state('app.home', {
                url: '/home',
                cache:false,
                views: {
                    'menuContent' :{
                        templateUrl: 'templates/home/home.html',
                        controller:'HomeCtrl as home',
                        cache:false,
                        resolve : {
                            accounts : ['Accounts',function(Accounts) {
                                return Accounts.getUserAccounts();
                            }],
                            connection: ['Socket', function(Socket) {
                                return Socket.connect();
                            }]
                        }
                    }
                }
            })
            .state('app.list', {
                url: '/home/:id',
                cache:false,
                views: {
                    'menuContent' :{
                        templateUrl: 'templates/home/list.html',
                        controller:'ListCtrl as list',
                        cache:false
                    }
                }
            })

            .state('app.item', {
                url: '/item/:index/:id',
                cache:false,
                views: {
                    'menuContent' :{
                        cache:false,
                        templateUrl: 'templates/home/item.html',
                        controller:'ItemCtrl as item'
                    }
                }
            });

    });
