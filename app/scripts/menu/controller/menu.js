'use strict';

angular.module('CleanBoxMobile.menu')
    .controller('MenuCtrl', function($scope, Auth, $location, $ionicSideMenuDelegate, ENV, Accounts, Help, $state, OAuth){

        var menu= this;
        this.logoutInProgress= false;

        this.logout = function() {
            $ionicSideMenuDelegate.toggleLeft(true);
            this.logoutInProgress=true;
            Auth.logout()
                .then(function() {
                $location.path('/');
                //$state.go('login');
                });
        };


        $scope.closeHelp = function() {
          Help.closeModal();
        };


        this.openHelp = function() {
          Help.openModal($scope);
        };

    });
