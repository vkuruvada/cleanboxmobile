/**
 * Created by venkatakuruvada on 10/8/14.
 */

angular.module('CleanBoxMobile.auth')
    .controller('LoginCtrl', function($scope, $location, Auth, $window, Help, Accounts){

        var login = this;
        this.user = {};
        this.errors = {};


        $scope.closeHelp = function() {
          Help.closeModal();
        };


        $scope.openHelp = function() {
          Help.openModal($scope);
        };

        this.login = function(form) {

            this.submitted = true;

            if(form.$valid) {

                this.loginInProgress=true;
                Auth.login({
                    email: this.user.email,
                    password: this.user.password
                })
                    .then( function() {
                        Accounts.deleteAllAccounts();
                        $location.url('/app/home');
                    })
                    .catch( function(err) {
                        err = err.data;
                        login.errors.other = err.message;
                        login.loginInProgress= false;
                    });
            }
        };

    });
