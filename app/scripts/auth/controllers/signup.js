'use strict';
/**
 * Created by venkatakuruvada on 10/31/14.
 */

angular.module('CleanBoxMobile.auth')
  .controller('SignUpCtrl', function($scope, Auth, $location, Help, Accounts){

      var signup= this;
      this.user ={};
      this.errors={};
      this.signUpInProgress = false;

      $scope.closeHelp = function() {
        Help.closeModal();
      };


      $scope.openHelp = function() {
        Help.openModal($scope);
      };

    this.register = function(form) {

        if(form.$valid) {
          Auth.createUser({
            name: this.user.name,
            email: this.user.email,
            password: this.user.password
          })
            .then( function() {
              // Account created, redirect to home
              Accounts.deleteAllAccounts();
              $location.path('app/home');
            })
            .catch( function(err) {
              err = err.data;
              signup.errors.other = err.message;

            });
        }
      }

  });
