'use strict';

angular.module('CleanBoxMobile.auth')
  .controller('ForgotCtrl', function($scope, Help, Auth) {

    var forgot= this;
    this.user={};
    this.error={};
    this.inProgress= false;

    $scope.closeHelp = function() {
      Help.closeModal();
    };


    $scope.openHelp = function() {
      Help.openModal($scope);
    };


    this.submit = function(form) {

      this.submitted = true;

      if(form.$valid) {
        forgot.inProgress= true;
        Auth.reset({
          email: forgot.user.email
        })
          .then( function(response) {
            forgot.inProgress= false;
            forgot.error = response.data;
          })
          .catch( function(response) {
            forgot.submitted = false;
            forgot.inProgress= false;
            forgot.error = response.data;
          });
      }
    };


  });
