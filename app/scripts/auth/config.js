'use strict';


/**
 * Created by venkatakuruvada on 10/8/14.
 */


angular.module('CleanBoxMobile.auth',[])
    .config(function($stateProvider, $urlRouterProvider,ENV){

        $stateProvider
            .state('login',{
                url:'/app/login',
                templateUrl:'templates/auth/login.html',
                controller:'LoginCtrl as login',
                cache:false
            })
            .state('signup',{
                url:'/app/signup',
                templateUrl:'templates/auth/signup.html',
                controller:'SignUpCtrl as signup'
            })
          .state('forgot',{
            url:'/app/forgot',
            cache:false,
            templateUrl:'templates/auth/forgot.html',
            controller:'ForgotCtrl as forgot'
            })
            .state('google', {
                url:ENV.apiEndpoint+'/auth/google',
                cache:false,
                resolve : {
                    auth : ['$window',function($window) {
                        $window.open('/auth/google','_self');
                    }]
                }
            }).
            state('yahoo', {
                url:ENV.apiEndpoint+'/auth/yahoo',
                cache:false,
                resolve : {
                    auth :['$window', function($window) {
                        $window.open('/auth/yahoo','_self');
                    }]
                }
            });

    });
