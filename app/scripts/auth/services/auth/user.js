'use strict';

angular.module('CleanBoxMobile.auth')
  .factory('User', function ($resource, ENV) {
    return $resource(ENV.apiEndpoint+'/api/users/:id', {
      id: '@id'
    }, { //parameters default
      update: {
        method: 'PUT',
        params: {}
      },
      get: {
        method: 'GET',
        params: {
          id:'me'
        }
      }
	  });
  });
