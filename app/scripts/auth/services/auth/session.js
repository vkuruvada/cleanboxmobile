'use strict';

angular.module('CleanBoxMobile.auth')
  .factory('Session', function ($resource,ENV) {
    return $resource(ENV.apiEndpoint+'/api/session/');
  });
