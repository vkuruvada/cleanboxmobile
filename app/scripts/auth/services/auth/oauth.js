'use strict';

angular.module('CleanBoxMobile.auth')
  .factory('OAuth', function($q, $state, ENV) {

      var gWindow= null;
      var deferred = null;

      return  {

          addAccount : function() {


            var me = this;
            deferred = $q.defer();

            gWindow = window.open(ENV.apiEndpoint+'/auth/google', '_blank', 'location=yes');
            gWindow.addEventListener('loadstop', me.fLoadStop);
            gWindow.addEventListener('exit', me.fExit);

            return deferred.promise;
          },

        fLoadStop : function(event) {

            var url = event.url;
            var name= url.substr(0,30);
            if(name.indexOf(ENV.appName+'.')>-1) {
              setTimeout(function() {
                gWindow.close();
              }, 10);
              deferred.resolve();
            }
          },

          fExit : function() {

            var me = this;
            gWindow.removeEventListener('loadstop', me.fLoadStop);
            gWindow.removeEventListener('exit', me.fExit);
            setTimeout(function() {
              gWindow.close();
            }, 10);
            deferred.resolve();
          }
      }

  });
