'use strict';

angular.module('CleanBoxMobile.auth')
  .factory('Auth', function Auth($location, $rootScope, Session, User, $cookieStore, $http, ENV) {

    // Get currentUser from cookie
    $rootScope.currentUser = $cookieStore.get('user') || null;
    $cookieStore.remove('user');

    return {

      /**
       * Authenticate user
       *
       * @param  {Object}   user     - login info
       * @param  {Function} callback - optional
       * @return {Promise}
       */
      login: function(user, callback) {
        var cb = callback || angular.noop;

        return Session.save({
          email: user.email,
          password: user.password
        }, function(user) {
          $rootScope.currentUser = user;
          return cb();
        }, function(err) {
          return cb(err);
        }).$promise;
      },

      /**
       * reset password req
       * @param user
       * @param callback
       * @returns
       */
      reset:function(user, callback) {
        var cb = callback || angular.noop;

        return $http({
          url:ENV.apiEndpoint+'/forgot',
          method:'POST',
          data:{email:user.email}
        });
      },


      saveResetPassword : function(params, callback) {

        return $http({
          url:ENV.apiEndpoint+'/reset',
          method:'POST',
          data:params
        });
      },

      /**
       * Unauthenticate user
       *
       * @param  {Function} callback - optional
       * @return {Promise}
       */
      logout: function(callback) {
        var cb = callback || angular.noop;

        return Session.delete(function() {
            $rootScope.currentUser = null;
            return cb();
          },
          function(err) {
            return cb(err);
          }).$promise;
      },

      /**
       * Create a new user
       *
       * @param  {Object}   user     - user info
       * @param  {Function} callback - optional
       * @return {Promise}
       */
      createUser: function(user, callback) {
        var cb = callback || angular.noop;

        return User.save(user,
          function(user) {
            $rootScope.currentUser = user;
            return cb(user);
          },
          function(err) {
            return cb(err);
          }).$promise;
      },

      /**
       * Change password
       *
       * @param  {String}   oldPassword
       * @param  {String}   newPassword
       * @param  {Function} callback    - optional
       * @return {Promise}
       */
      changePassword: function(oldPassword, newPassword, callback) {
        var cb = callback || angular.noop;

        return User.update({
          oldPassword: oldPassword,
          newPassword: newPassword
        }, function(user) {
          return cb(user);
        }, function(err) {
          return cb(err);
        }).$promise;
      },

      setFilter: function(filter, callback) {

        var cb = callback || angular.noop;

        return $http({
          url:ENV.apiEndpoint+'/api/users/filter',
          method:'POST',
          data:{filter:filter}
        });
      },

      getFilter: function() {

        return $http({
          url:ENV.apiEndpoint+'/api/users/filter',
          method:'GET',
          data:{id:$rootScope.currentUser._id}
        });
      },

      /**
       * Gets all available info on authenticated user
       *
       * @return {Object} user
       */
      currentUser: function() {
        return User.get();
      },

      /**
       * Simple check to see if a user is logged in
       *
       * @return {Boolean}
       */
      isLoggedIn: function() {
        var user = $rootScope.currentUser;
        return !!user;
      }
    };
  });
