'use strict';

angular.module('CleanBoxMobile.auth')
    .factory('Accounts', function($http , $rootScope, Socket, ENV, $state) {

        var accounts=null,
            statusFetched=false;

        return  {
            getUserAccounts: function () {
                if(accounts) {
                    return accounts;
                }
                var me = this;
                return $http.get(ENV.apiEndpoint+'/accounts').then(function(response) {

                        var data = response.data;
                        if((data instanceof Array)) {
                            accounts = data;
                        } else {
                          //error fetching accounts
                         // console.log('Problem fetching accounts info.. please try again');
                        }

                })
            },

            getAccountStats: function (account) {

                return  $http({
                    url: ENV.apiEndpoint+'/accountsstats',
                    method: "GET",
                    params: {id: account._id}
                });
            },

            /**
             * @deprecated
             *
             */
            getAllAccountsStats : function() {

                return $http.get(ENV.apiEndpoint+'/allaccountstats').then(function(response){
                    accounts =response.data;
                });
            },

            getAccountById : function(id) {

                if(!accounts || !(accounts instanceof Array)) {
                  $state.go('app.home');
                  return;
                }

                for(var i=0;i<accounts.length ; i++) {
                    if(accounts[i]._id == id) {
                        return accounts[i];
                    }
                }
                return null;
            },

            updateAccounts : function(newAccounts) {
                accounts = newAccounts;
            },

            deleteAllAccounts : function() {
                accounts=null;
            },

            getStatusFetched : function(){
                return statusFetched;
            },

            setStatusFetched : function(value) {
                statusFetched = value;
            },

            fetchFolders : function(currentUser) {
                // fetch status dynamically
                if(accounts && accounts instanceof Array) {
                    accounts.forEach(function(account){

                        Socket.emit('accountfolders', {
                            user :currentUser,
                            account : account
                        });
                    });

                    Socket.on('account:folders', this.onAccountFoldersReceived, this);
                }
            },

            onAccountFoldersReceived : function(acc) {

                this.getUserAccounts().forEach(function(account){
                    if(acc._id == account._id) {
                        account.folders=acc.folders;
                    }
                });

                this.updateAccounts(this.getUserAccounts());
                $rootScope.$broadcast('onFoldersFetched');
            },

            fetchStats : function(currentUser) {
                // fetch status dynamically
                if(accounts) {

                    if(currentUser && currentUser.$promise) {
                      delete currentUser.$promise;
                    }

                    accounts.forEach(function(account){

                        Socket.emit('accountstatus' , {
                            user :currentUser,
                            account : account
                        });


                        Socket.emit('accountfolders', {
                            user :currentUser,
                            account : account
                        });
                    });

                    this.setStatusFetched(true);
                    Socket.on('accountstatus', this.onAccountStatusReceived, this);
                    Socket.on('accountquota', this.onAccountQuotaReceived, this);
                }
            },

            // on each status receieved update the UI
            onAccountStatusReceived : function(acc) {

                this.getUserAccounts().forEach(function(account){
                    if(acc._id == account._id) {
                        account.stats=acc.stats;
                    }
                });

                this.updateAccounts(this.getUserAccounts());
                $rootScope.$broadcast('onStatusUpdated');
            },

          // on each status receieved update the UI
          onAccountQuotaReceived : function(acc) {

            this.getUserAccounts().forEach(function(account){
              if(acc._id == account._id) {
                account.quota=acc.quota;
              }
            });

            this.updateAccounts(this.getUserAccounts());
            $rootScope.$broadcast('onStatusUpdated');
          },

            fetchHistory : function(currentUser, account) {

                //fetch history
                Socket.emit('account:history', {
                    user :currentUser,
                    account : account
                });

                Socket.on('account:historydata', this.onHistoryFetched, this);
            },

            onHistoryFetched : function(data) {

                accounts.forEach(function(account){

                    if(account._id === data._id) {
                        account.history = data.history;
                        account.totalManaged =data.totalManaged;
                    }
                });
            }
        }
    });
