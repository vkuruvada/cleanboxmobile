
angular.module('CleanBoxMobile.move',[])
    .factory('MoveFactory', function(Socket, $ionicModal){

        var modalInstance=null;

        return {

            getTreeConfig : function() {
                return {
                    nodeChildren: "children",
                    dirSelectable: false,
                    injectClasses: {
                        ul: "a1",
                        li: "a2",
                        liSelected: "a7",
                        iExpanded: "a3",
                        iCollapsed: "a4",
                        iLeaf: "a5",
                        label: "tree-item",
                        labelSelected: "a8"
                    }
                };
            },

            doMove : function(currentUser, account, filter, folder, callback, scope) {

                Socket.emit('movemessages' , {
                    user :currentUser,
                    account : account,
                    filter :filter ,
                    folderName :folder
                });


              var successfn = function() {
                Socket.remove('move:success');
                callback.call(scope);
              };

              Socket.on('move:success', successfn, this);
            },

            createMoveModal: function(scope) {

              var me = this;
              $ionicModal.fromTemplateUrl('templates/move/move.html', {
                scope: scope,
                animation: 'slide-in-up'
              }).then(function(modal) {
                modalInstance = modal;
              });

              //Cleanup the modal when we're done with it!
              scope.$on('$destroy', function() {
              });

            },

            openModal : function() {
              modalInstance.show();
            },

            closeModal : function() {
              modalInstance.hide();
            }

        }

    });
