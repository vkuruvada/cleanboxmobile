'use strict';

/**
 * Created by venkatakuruvada on 11/2/14.
 */

angular.module('CleanBoxMobile.help',[])
  .factory('Help', function($ionicModal, $http, $rootScope) {

    var modalInstance = null;

    var help ={};
    $http.get('helpconfig.json').success(function(response) {
      $rootScope.helpData = response.help;
    });
    return {

      createModel: function (scope) {

        var me = this;
        var instance = $ionicModal.fromTemplateUrl('templates/help/help.html', {
          scope: scope,
          animation: 'slide-in-up'
        }).then(function (modal) {
          modalInstance = modal;
        });

        scope.$on('$destroy', function () {
          modalInstance.remove();
          modalInstance = null;
        });

        return instance;

      },

      openModal: function (scope) {
        if (!modalInstance) {
          this.createModel(scope).then(function () {
            modalInstance.show();
          });
        } else {
          modalInstance.show();
        }
      },

      closeModal: function () {
        modalInstance.hide();
      }

    }

  });
