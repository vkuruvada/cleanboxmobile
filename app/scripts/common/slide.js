'use strict';

angular.module('CleanBoxMobile.common')
    .directive('slide', function(){

        return {
            restrict:'EA',
            controller: function($scope, Accounts, Socket, Messages, MessagesStore){

                $scope.archiveInProgress=false;
                $scope.doneArchiving=false;

                $scope.handleDrag = function(element, e) {

                    element[0].style.webkitTransitionDuration="0s";
                    element[0].style.webkitTransitionDuration="";


                    var windowMidWidth = window.innerWidth/3;
                    var vals = $scope.parseTransform(element[0].style.webkitTransform);
                    if(e.type =='dragleft' || e.type == 'dragright') {
                        element[0].style.webkitTransform="translate3d("+e.gesture.deltaX+"px,"+vals.join()+")";

                        if(e.type == 'dragleft' && Math.abs(e.gesture.deltaX)>windowMidWidth) {
                            element.addClass('positive-bg');
                        } else {
                            element.removeClass('positive-bg');
                        }

                        if(e.type == 'dragright' && Math.abs(e.gesture.deltaX)>windowMidWidth) {
                            element.addClass('calm-bg');
                        } else  {
                            element.removeClass('calm-bg');
                        }
                    }

                    if(e.type == 'release') {

                        element[0].style.webkitTransitionDuration="1.2s";
                        element[0].style.webkitTransitionDuration="linear";

                        if(element.hasClass('positive-bg')) {
                            if(!$scope.archiveInProgress && !$scope.doneArchiving){
                               // $scope.triggerArchive();
                            }

                            element[0].style.webkitTransform="translate3d(-"+window.innerWidth+"px,"+vals.join()+")";
                        }

                        else if(element.hasClass('calm-bg')) {
                            if(!$scope.archiveInProgress && !$scope.doneArchiving){
                               // $scope.triggerArchive();
                            }

                            element[0].style.webkitTransform="translate3d("+window.innerWidth+"px,"+vals.join()+")";
                        }

                        else {

                            element.removeClass('positive-bg');
                            element.removeClass('calm-bg');

                            element[0].style.webkitTransform = "translate3d( 0px," + vals.join() + ")";
                        }
                    }
                };

                $scope.parseTransform = function(transformStr) {
                    var str = transformStr.substring(12, transformStr.length-1);
                    return str.split(',').slice(1);
                }


                //archive
                $scope.triggerArchive = function() {

                    $scope.archiveInProgress=true;
                    Messages.triggerArchive(this.constructDeleteList(), $scope.currentUser, $scope.account)
                };

                // This function creates the search  criteria to delete
                $scope.constructDeleteList = function() {

                    var criteria = [{
                        address:$scope.message.address,
                        count:$scope.message.count
                    }];
                    return criteria;
                };

                //delete/move
                $scope.onArchiveComplete = function() {

                    $scope.$apply(function(){
                        $scope.archiveInProgress=false;
                        $scope.removeSelectedItems();
                    });

                };

                $scope.removeSelectedItems = function(){

                    if($scope.doneArchiving){
                        return;
                    }

                    MessagesStore.getMessages($scope.account.id);
                    var messages = Messages.getAllMessages();
                    var index =  messages.indexOf($scope.message);
                    if(index>-1)
                        messages.splice(index, 1);

                    MessagesStore.addMessages($scope.account.id, messages);
                    $scope.doneArchiving=true;
                };

                Socket.on('archiveComplete', $scope.onArchiveComplete, $scope);



            },
            scope:{
                message:'=',
                account:'=',
                currentUser:'='
            },
            link: function(scope, element, attr) {

                var el= element;
                var handleDrag = function(e) {

                    scope.handleDrag(el, event);
                };

                var slideGesture = ionic.onGesture('release dragleft dragright swipeleft swiperight',
                                                        handleDrag, element[0]);

                scope.$on('$destroy', function(){

                    ionic.offGesture(slideGesture, 'release dragleft dragright swipeleft swiperight',
                                                        scope.handleDrag);
                });
            }
        }
    });
