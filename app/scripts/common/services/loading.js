'use strict';
/**
 * Created by venkatakuruvada on 10/25/14.
 */


angular.module('CleanBoxMobile.common')
  .service('Loading', ['$ionicLoading',function($ionicLoading){


    this.showLoading = function(text) {
      $ionicLoading.show({
        template: text
      });
    };
    this.hideLoading = function(){
      $ionicLoading.hide();
    };

  }]);
