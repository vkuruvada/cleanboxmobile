'use strict';

angular.module('CleanBoxMobile.common',[])
    .run(function($rootScope, Auth, $location){

        var loginEscapeUrl = ['/','/app/login','/app/signup'];
        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams){

                if(!Auth.isLoggedIn()) {
                    if(loginEscapeUrl.indexOf(toState.url)>-1 || toState.url.indexOf('/resetpassword')>-1){
                        //allow this paths
                    } else {
                        $location.path('/');
                    }

                }
        });

        $rootScope.$on('$stateChangeSuccess',
            function(event, toState, toParams, fromState, fromParams){

                if((loginEscapeUrl.indexOf(toState.url)>-1 || toState.url.indexOf('/resetpassword')>-1) && Auth.isLoggedIn()) {
                    $location.path('/app/home');
                }
        });

        $rootScope.$on('$stateChangeError',
            function(event, toState, toParams, fromState, fromParams, error){
        });



    });
