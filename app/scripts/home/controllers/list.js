'use strict';

/**
 * Created by venkatakuruvada on 10/11/14.
 */

angular.module('CleanBoxMobile.home')
    .controller('ListCtrl', function($scope, Accounts, $stateParams, Socket, Messages,
                                     $state, MessagesStore, Loading, MoveFactory, $ionicListDelegate){

        this.account = Accounts.getAccountById($stateParams.id);
        this.search ='';
        var list = this;
        this.title="Loading...";
        this.data = {
          multiMode:false
        };

        if(!this.account) {
            return $state.go('app.home');
        }

        if(MessagesStore.hasMessages(list.account._id)){

            $scope.info = {
                clenInProgress:false,
                messages:MessagesStore.getMessages(list.account._id)
            };
            list.title = list.account.name;

        } else  {

            Socket.emit('clean' , {
                user :$scope.currentUser,
                account : this.account
            });


            $scope.info = {
                total : 0,completed:0,clenInProgress:false,account:null,
                messages:[]
            };

            Accounts.fetchHistory($scope.currentUser, this.account);
        }



        this.onMessages = function(newData) {

            $scope.$apply(function() {

                $scope.info.messages = newData;

                $scope.info.messages.sort(function(a,b){
                    return b.count- a.count;
                });

                Messages.addMessages($scope.info.messages);

                // update progressbar info
                var count =0;
                $scope.info.messages.forEach(function(element){
                    count=count+element.count;
                });
                $scope.info.completed=count;
                $scope.totalServerItems = newData.length;
            });
        };

        Socket.on('messages', this.onMessages, this);



        this.getMessages = function() {

            if(this.search) {

                return $scope.info.messages.filter(function (item) {
                        return item.address.toLowerCase().indexOf(list.search.toLowerCase()) > -1;
                });
            } else {

                return $scope.info.messages;
            }

        };

        this.clearSearch = function() {
            this.search ='';
        };


        this.onFetchCompleted = function(totalFetched){

            MessagesStore.addMessages(list.account._id, $scope.info.messages);
            $scope.$apply(function(){
                list.title = list.account.name;
                $scope.info.cleanInProgress=false;
            });
        };

        Socket.on('messages:done', this.onFetchCompleted , this);

        this.getItemHeight = function(item, index) {
            //Make evenly indexed items be 10px taller, for the sake of example
            return (index % 2) === 0 ? 53 :53;
        };

        this.doArchive = function(message, index) {
            this.triggerArchive(message, index);
        };

        this.doTrash = function(message, index) {
          this.trash(message, index);
        };

        this.doDelete = function(message, index) {
          this.triggerArchive(message, index);
        };

        this.toMove = function(message, index) {
          $scope.selected.message=message;
          $scope.selected.index=index;
          MoveFactory.openModal();
        };


      //archive
      this.triggerArchive = function(message, index) {

        Loading.showLoading('Archiving...');
        this.archiveInProgress=true;
        Messages.triggerArchive(this.constructDeleteList(message), $scope.currentUser, this.account,
          function() {

            $scope.$apply(function(){
              list.archiveInProgress=false;
              list.removeSelectedItems(index);
            });

        }, this);
      };


      this.trash = function(message, index) {

        Loading.showLoading('Deleting...');
        this.trashInProgress=true;
        Messages.trash(this.constructDeleteList(message), $scope.currentUser, this.account,
            function() {

              $scope.$apply(function(){
                list.trashInProgress=false;
                list.removeSelectedItems(index);
              });

            }, this);
      };




      // This function creates the search  criteria to delete
      this.constructDeleteList = function(message) {

        var criteria = [{
          address:message.address,
          count:message.count
          }];
        return criteria;
      };

      this.removeSelectedItems = function(index){

        MessagesStore.getMessages(this.account._id);
        var messages = Messages.getAllMessages();
        messages.splice(index, 1);

        MessagesStore.addMessages(this.account._id, messages);

        Accounts.fetchHistory($scope.currentUser, this.account);
        $scope.selected = {
          item: null,
          message:null,
          index:null
        };
        Loading.hideLoading();
        $ionicListDelegate.closeOptionButtons();

      };



    $scope.items = this.account.folders;
    $scope.moveInProgress=false;
    $scope.selected = {
      item: null,
      message:null,
      index:null
    };

    //Move
    MoveFactory.createMoveModal($scope);

    $scope.openMove = function() {
      MoveFactory.openModal();
    };

    $scope.moveToFolder = function () { //movemessages

      MoveFactory.doMove($scope.currentUser, list.account, list.constructDeleteList($scope.selected.message),
          $scope.node['name'], function() {

          $scope.clearSelection();
          $scope.$apply(function(){
            $scope.moveInProgress= false;
            list.removeSelectedItems($scope.selected.index);
            MoveFactory.closeModal();

          });

        }, list);
      $scope.moveInProgress= true;
    };


    $scope.cancel = function () {
      $scope.selected = {
        message:null,
        index:null
      };
      MoveFactory.closeModal();
    };


    $scope.treeOptions = MoveFactory.getTreeConfig();

    $scope.onSelection = function(node) {
      $scope.node =node;
    };

    $scope.clearSelection = function() {
      $scope.node= undefined;
    }

    });
