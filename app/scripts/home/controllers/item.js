'use strict';

/**
 * Created by venkatakuruvada on 10/15/14.
 */

angular.module('CleanBoxMobile.home')
    .controller('ItemCtrl', function($scope, Messages, Accounts, $stateParams,
                                     MessagesStore, Socket,  $ionicHistory, MoveFactory, Loading){

        var item = this;
        var messages = Messages.getAllMessages();
        this.message = messages[$stateParams.index];
        this.account = Accounts.getAccountById($stateParams.id);
        this.archiveInProgress=false;
        this.deleteInProgress= false;

        this.movedList =[];

        this.hasMovedHistory = false;

        this.isMoved = function() {
            return this.hasMovedHistory;
        };


        this.findMoves = function() {

            if(this.account && this.account.history){

                if(!this.account.history[this.message.address]) {
                    return false;
                }
                for(var f in this.account.history[this.message.address].moved) {

                    this.hasMovedHistory=true;
                    this.movedList.push({
                        folder:f,
                        count:this.account.history[this.message.address].moved[f]
                    });
                }
            }
        };
        this.findMoves();


        //archive
        this.triggerArchive = function() {

            this.archiveInProgress=true;
            Loading.showLoading('Please wait...');
            Messages.triggerArchive(this.constructDeleteList(), $scope.currentUser, this.account,
                                    this.onArchiveComplete, this);
        };



        this.triggerDelete = function() {

          this.deleteInProgress=true;
          Loading.showLoading('Deleting...');
          Messages.trash(this.constructDeleteList(), $scope.currentUser, this.account,
            this.onArchiveComplete, this);
        };


        // This function creates the search  criteria to delete
        this.constructDeleteList = function() {

            var criteria = [{
                address:this.message.address,
                count:this.message.count
            }];
            return criteria;
        };

        //delete/move
        this.onArchiveComplete = function() {

            $scope.$apply(function(){
                item.archiveInProgress=false;
                item.deleteInProgress=true;
                item.removeSelectedItems();
                Loading.hideLoading();
            });

        };

        this.removeSelectedItems = function(){

            MessagesStore.getMessages(this.account._id);
            var messages = Messages.getAllMessages();
            messages.splice($stateParams.index, 1);

            MessagesStore.addMessages(this.account._id, messages);

            Accounts.fetchHistory($scope.currentUser, this.account);
            $ionicHistory.goBack();
        };


        $scope.items = this.account.folders;
        $scope.moveInProgress=false;
        $scope.selected = {
            item: null
        };

        MoveFactory.createMoveModal($scope);

        $scope.openMove = function() {
          MoveFactory.openModal();
        };

       $scope.moveToFolder = function () { //movemessages

        MoveFactory.doMove($scope.currentUser, item.account, item.constructDeleteList(),
          $scope.selected.item['name'], function() {

            $scope.$apply(function(){

              item.moveInProgress= false;
              item.removeSelectedItems();
              MoveFactory.closeModal();
            });


          }, item);
          $scope.moveInProgress= true;
        };

        $scope.cancel = function () {
          MoveFactory.closeModal();
        };


        $scope.treeOptions = MoveFactory.getTreeConfig();



    });
