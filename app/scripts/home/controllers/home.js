'use strict';

/**
 * Created by venkatakuruvada on 10/11/14.
 */

angular.module('CleanBoxMobile.home')
    .controller('HomeCtrl',function($location, Accounts, Socket, $scope, MessagesStore, $state, ENV, OAuth){

        var home = this;
        this.accounts = Accounts.getUserAccounts();

        // remove all previously cached messages
        MessagesStore.removeAll();


        if(!Socket.isConnected()) {
          Socket.reconnect();
        };


        this.initSetup = function() {


          home.title= "Fetching status ....";
          home.loadCompleted = false;

          setTimeout(function(){
            if(!home.loadCompleted) {
              home.doRefresh();
            }
          },5000);

        };


        if(this.accounts.length>0) {
          this.initSetup();
        }


        // fetch status dynamically
        Accounts.fetchStats($scope.currentUser);
        $scope.$on('onStatusUpdated', function(){
            home.title= "Accounts";
            home.loadCompleted=true;
            $scope.$apply();
        });

        Accounts.fetchFolders($scope.currentUser);
        $scope.$on('onFoldersFetched', function(){
            $scope.$apply();
        });

        this.doRefresh = function() {

            home.initSetup();
            Accounts.deleteAllAccounts();
            Accounts.getUserAccounts().then(function(){
                home.accounts = Accounts.getUserAccounts();
                Accounts.fetchStats($scope.currentUser);
            }).finally(function() {
                // Stop the ion-refresher from spinning
                $scope.$broadcast('scroll.refreshComplete');
            });

        };

        this.onAddAccount = function(type) {
            $location.path('/auth/'+type);
        };

        this.setActive = function(email) {
            this.active = email;
        };

        this.isActive = function(email) {
            return (this.active === email);
        };


        this.doNavigate = function(account) {

          if(account.stats && account.stats>0) {

              $state.go('app.list',{id:account._id});
          } else if(account.stats && account.stats === 0){

              alert('Account already clean');
          } else {

             alert('Stats not available ... please try refreshing (pull to refresh)');
          }

        };



        this.addAccount = function() {

          OAuth.addAccount().then(function() {

            Accounts.deleteAllAccounts();
            setTimeout(function(){
              home.doRefresh();
            },100);
          });

        };


    });
