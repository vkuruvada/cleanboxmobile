'use strict';

angular.module('CleanBoxMobile.home')
    .factory('MessagesStore', function(){

        var store= {};

        return {

            addMessages : function(id, messages) {
                store[id]= messages;
            },
            removeMessages: function(id) {
                delete store[id];
            },
            removeAll : function() {
                store ={};
            },
            getMessages : function(id) {
                return store[id];
            },
            hasMessages : function(id){
                return !!store[id];
            }
        }
    });