'use strict';


/**
 * Created by venkatakuruvada on 9/13/14.
 */

angular.module('CleanBoxMobile.home')
    .factory('Messages',function($rootScope, Socket) {

        var messages = [];
        return  {

            addMessages  : function(msgs) {
                messages = msgs;
            },

            getAllMessages : function() {
                return messages;
            },

            deleteAllMessages  : function() {
                messages =[];
            },

            //archive
            triggerArchive : function(criteria, currentUser, account, callback, scope) {
                Socket.emit('archive' , {
                filter :criteria ,
                user :currentUser,
                account : account

              });

              var successfn = function() {
                Socket.remove('archiveComplete');
                callback.call(scope);
              };

              Socket.on('archiveComplete', successfn, this);
          },


          trash : function(criteria, currentUser, account, callback, scope) {


            Socket.emit('movemessages' , {
              filter :criteria ,
              user :currentUser,
              account : account,
              folderName :'[Gmail]/Trash'
            });

            var successfn = function() {
              Socket.remove('move:success');
              callback.call(scope);
            };

            Socket.on('move:success', successfn, this);
          }


        };
    });
