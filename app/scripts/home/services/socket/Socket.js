'use strict';

/**
 * Created by venkatakuruvada on 9/13/14.
 */

angular.module('CleanBoxMobile.home')
    .factory('Socket', function(ENV, $q) {

        var connected=false;
        var connectionCount =0;
        var defer = $q.defer();

        var socket = io(ENV.socketPath || ENV.apiEndpoint , {
          'max reconnection attempts' : 5
        }); //http://localhost/


        var reconnect = function() {

          // try 5 times;
          if(connectionCount<=5 && !connected) {
            console.log('reconnecting');
            socket.io.reconnect();
            connectionCount++;
          } else {
            //alert('connection issue please. check your data connection')
          }
        };

        socket.on('connect',function(){
            console.log('connected');
            connected = true;
            connectionCount=0;
            defer.resolve();
        });
        socket.on('error', function (err) {
            if(!connected)
              reconnect();
        });

        socket.on('reconnecting', function() {
          if(!connected)
            reconnect();
        });


        socket.on('disconnect', function() {

          connected = false;
          console.log('disconnected');
          reconnect();

        });

    var registeredEvents = {};

        // execute all listeners
        function executeFn(eventName, result) {

            if(registeredEvents[eventName]) {
                 var listeners = registeredEvents[eventName];
                listeners.forEach(function(listener) {

                   listener.fn.call(listener.scope|| null , result);
                });

            }
        }

        return  {

            isConnected: function(fn) {
              return connected;
            },

            reconnect : function() {
              reconnect();
            },

            emit : function(evtName , data ) {

              socket.emit(evtName ,data);

            },

            on : function(evtName , fn , scope) {

                    if(!registeredEvents[evtName]) {
                        registeredEvents[evtName] =[];
                    }

                    //sanity check
                   var listenerexists = false;
                   registeredEvents[evtName].forEach(function(obj){
                       if(obj.fn == fn) {
                           listenerexists=true;
                       }
                   });

                    if(!listenerexists) {
                        registeredEvents[evtName].push({
                            fn:fn,
                            scope:scope
                        });
                        socket.on(evtName , executeFn.bind(this, evtName));
                    }

            },
            remove : function(listenerName) {

                var exists = false;
                registeredEvents[listenerName]=[];
                socket.removeAllListeners(listenerName);
            },
            removeAll : function() {
                registeredEvents ={};
            },


            connect: function() {

              return defer.promise;
            }
        }
    });
