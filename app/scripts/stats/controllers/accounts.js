'use strict';

/**
 * Created by venkatakuruvada on 10/18/14.
 */

angular.module('CleanBoxMobile.stats',[])
    .controller('AccountsStatCtrl', function($scope, Accounts, Socket, $state){

        var stats = this;
        this.accounts = Accounts.getUserAccounts();

        // fetch status dynamically
        Accounts.fetchStats($scope.currentUser);
        $scope.$on('onStatusUpdated', function(){
            $scope.$apply();
        });


        $scope.$on('onHistoryFetched', function(){

            $scope.$apply();
        });

        this.accounts.forEach(function(account){
            Accounts.fetchHistory($scope.currentUser, account);
        });


        this.doRefresh = function() {

            Accounts.deleteAllAccounts();

            Accounts.getUserAccounts().then(function(){
                stats.accounts = Accounts.getUserAccounts();
                Accounts.fetchStats($scope.currentUser);
                stats.accounts.forEach(function(account){
                    Accounts.fetchHistory($scope.currentUser, account);
                });

            }).finally(function() {
                // Stop the ion-refresher from spinning
                $scope.$broadcast('scroll.refreshComplete');
            });

        };

        this.doNavigate = function(account) {

          if(account.totalManaged && account.totalManaged>0) {
            $state.go('app.history',{id:account._id});
          } else  {
            alert('No history available');
          }
        }

  });
