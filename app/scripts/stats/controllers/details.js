'use strict';

/**
 * Created by venkatakuruvada on 10/19/14.
 */

angular.module('CleanBoxMobile.stats')
    .controller('DetailsCtrl', function($scope, Accounts, $stateParams){

        var details = this;
        this.account = Accounts.getAccountById($stateParams.id);

        this.getMovedString = function(moved) {

          var res=[];
          for(var i in moved) {
            res.push((i+'('+moved[i]+')'));
          }
          return res.join(',')
        };

        this.historyList =[];
        if(this.account.history) {

            for(var his in this.account.history) {

                this.historyList.push({
                    address:his,
                    archived:this.account.history[his].archived,
                    moved:details.getMovedString(this.account.history[his].moved)
                });
            }
        }

        this.getItemHeight = function(item, index) {
            //Make evenly indexed items be 10px taller, for the sake of example
            return (index % 2) === 0 ? 60 :60;
        };
    });
