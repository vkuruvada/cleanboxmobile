'use strict';
// Ionic Starter App, v0.9.20

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('CleanBoxMobile', ['ionic',
                                  'ngCookies',
                                  'ngResource',
                                  'config',
                                  'treeControl',
                                  'CleanBoxMobile.all',
                                  'ui.router'])

.run(function($ionicPlatform, $cookies) {
  $ionicPlatform.ready(function($cookies) {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    cordova.plugins.Keyboard.disableScroll(true);

  });
})

.config(function($urlRouterProvider) {

  // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/login');
});

